const inputs = document.querySelectorAll('#form div input'),
    celular = document.getElementById('phone'),
    correo = document.getElementById('email'),
    adver = document.getElementById('warning');

const expresionesRegulares = {
    numeros : /^([0-9])+$/,
    text : /^([a-zA-Z])+$/,
    caracteres : /^[^a-zA-Z\d\s]+$/,
    correo : /^([a-z\d]+[@]+[a-z]+\.[a-z]{2,})+$/,
    espacios : /\s/g
}
inputs.forEach(input=>{
    input.addEventListener('keyup', (e) => {   
        let valueInput = e.target.value;

        switch(e.target.id){
            
            case 'phone':
                input.value = valueInput.replace(expresionesRegulares.espacios, '');
                if(expresionesRegulares.numeros.test(e.target.value)){
                    if(e.target.value.length != 10){
                        adver.innerHTML = 'Un numero celular tiene 10 caracteres';
                    }else{
                        celular.style.border = 'none';
                        celular.style.borderBottom ='1px solid #35353588';
                        adver.innerHTML = '';
                    }
                }else{
                    celular.style.border = '2px solid red';
                    adver.innerHTML = 'El numero que ingreso no es correcto';
                }
            break;

            case 'user':
                input.value = valueInput.replace(expresionesRegulares.espacios, '').replace(expresionesRegulares.caracteres, '');
            break;

            case 'email':
                if(expresionesRegulares.correo.test(e.target.value)){
                    correo.style.border = 'none';
                    correo.style.borderBottom ='1px solid #35353588';
                    adver.innerHTML = '';
                }else{
                    correo.style.border = '2px solid red';
                    adver.innerHTML = 'El correo que ingreso no es correcto';
                }
            break;
            
            case 'password':
                input.value = valueInput.replace(expresionesRegulares.espacios, '');
                if(e.target.value.length < 8){
                    adver.innerHTML = 'Solo se permiten contraseñas mayor a 8 caracteres';
                }else{
                    adver.innerHTML = '';
                }
            break;


        }
    })
});


form.addEventListener("submit", e=>{
    e.preventDefault();
    
    nombre =document.getElementById('name');
    apellidos = document.getElementById('lastname');
    usuario = document.getElementById('user');
    contrasena = document.getElementById('password');
    condiciones = document.getElementById('condiciones');
    if(nombre.value== ""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(apellidos.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(celular.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(usuario.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(correo.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(contrasena.value==""){
        adver.innerHTML = "Debe ingresar todos los campos";
    }else if(condiciones.checked==false){
        adver.innerHTML = 'Debe aceptar los terminos y condiciones';
    }else{
        alert("Registrado con exito");
    }
    
})


